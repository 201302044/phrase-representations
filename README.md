# phrase-representations
- Phrase representations based on context information.
- Generates Graph of the phrases where two phrases have an edge if the PMI of these two phrases are greater then certain threshold.
- This graph will be later used to train the siamese network using contrastive loss.
