#!/usr/bin/python

import numpy as np
import os
import sys
import random
import pickle

def rescale(inp):
   res = []
   _min = min(inp)
   _max = max(inp)
   a = -1
   b = 1
   for i in inp:
      k = ((b - a) * (i - _min)) / float(_max - _min)
      k += a
      res.append(k)
   return inp

try:
  fin = sys.argv[1]
  fout = sys.argv[2]
  maxAllowed = int(sys.argv[3])
except Exception:
  print 'Usage: python <reform.py> <input pickle file> <output pickle file> <number of negative random samples>'
  exit(0)

sys.stderr.write('Loading %s file\n' % fin)
sys.stderr.flush()
f = open(fin, 'rb')
phrasePairs = pickle.load(f)
f.close()

sys.stderr.write('Generating training set\n')
sys.stderr.flush()
similarity = phrasePairs
phrasePairs = []
l = 0
total = len(similarity)
allPhrases = []

for i in similarity:
   allPhrases.append(i)
   l += 1
   tmp = []
   for j in similarity[i]:
      tmp.append(j[1])
   tmp = rescale(tmp)
   assert(len(tmp) == len(similarity[i]))
   L = 0
   for j in tmp:
      if(j == 0):
         break
      else:
         L += 1
   oth = []
   LL = len(tmp)
   for j in xrange(L):
      print i, similarity[i][j][0], tmp[j]
      phrasePairs.append((i, similarity[i][j][0], tmp[j]))
      it = 105
   while((len(oth) < maxAllowed) and it):
      it -= 1
      idx = random.randint(L, LL - 1)
      if(idx not in oth):
         oth.append(idx)
   for j in oth:
      assert(tmp[j] == 0)
      print i, similarity[i][j][0], tmp[j]
      phrasePairs.append((i, similarity[i][j][0], tmp[j]))
   sys.stderr.write('  Processed %05d/%05d (%.2f%%)     \r' % (l, total, (float(l) / float(total)) * 100.0))
   sys.stderr.flush()

sys.stderr.write('\n')
random.shuffle(phrasePairs)
f = open(fout, 'wb')
pickle.dump(phrasePairs, f)
f.close()

f = open('allPhrases.pkl', 'wb')
pickle.dump(allPhrases, f)
f.close()
