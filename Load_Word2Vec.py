#!/usr/bin/python

import numpy as np
import os
import sys
import tensorflow as tf
from keras.datasets import mnist
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers import LSTM
from keras.layers import Input, merge
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Input, Lambda
from keras.optimizers import SGD, RMSprop, Adam, Nadam, Adadelta
from keras.callbacks import *
from keras.utils import np_utils
from keras import backend as K
from keras import callbacks
import random
import pickle
from gensim.models import Word2Vec

sess = tf.Session()
K.set_session(sess)

##################################################################
try:
   word2vecfname = sys.argv[1]
except Exception:
   print 'Usage: python <Load_Word2Vec.py> <word2vec_model>'
   exit(0)

##################################################################
print 'Loading word2vec model'
w2vmodel = Word2Vec.load(word2vecfname)

while True:
  try:
    ty = raw_input('type: ')
    if(ty == '1'):
      x = raw_input('Word: ')
      print w2vmodel[x]
    else:
      x = raw_input('Word: ')
      p = w2vmodel.most_similar(x)
      for i in p:
        print i
  except Exception as e:
    print e
"""
print model1.predict(np.array([[1, 2, 3]]))
print model2.predict(np.array([[1, 2, 3]]))

print model1.predict(np.array([[4, 5, 6]]))
print model2.predict(np.array([[4, 5, 6]]))

print model1.predict(np.array([[7, 8, 9]]))
print model2.predict(np.array([[7, 8, 9]]))

print model.predict([X1_train, X2_train])

exit(0)
"""
