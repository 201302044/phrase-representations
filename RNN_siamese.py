#!/usr/bin/python

import numpy as np
import os
import sys
import tensorflow as tf
from keras.datasets import mnist
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers import LSTM
from keras.layers import Input, merge
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Input, Lambda
from keras.optimizers import SGD, RMSprop, Adam, Nadam, Adadelta
from keras.callbacks import *
from keras.utils import np_utils
from keras import backend as K
from keras import callbacks
import random
import pickle
from gensim.models import Word2Vec

sess = tf.Session()
K.set_session(sess)

##################################################################
try:
   fname = sys.argv[1]
   word2vecfname = sys.argv[2]
   nbe = int(sys.argv[3])
   lang = sys.argv[4]
except Exception:
   print 'Usage: python <RNN_siamese.py> <data_pkl> <word2vec_model> <epoch> <lang>'
   exit(0)

##################################################################
print 'Loading word2vec model'
w2vmodel = Word2Vec.load(word2vecfname)

print 'Loading phrase pairs'
f = open(fname, 'rb')
phrasePairs = pickle.load(f)
f.close()

Vocab = {}
data = []
temp = set()
##################################################################
# Change these parameters accordingly.
VOCABULARY = len(Vocab)
NB_EPOCH = nbe
BATCH_SIZE = 256
INPUT_LENGTH = 3
EMBEDDING_DIMENSION = 64
##################################################################

def transformData(x):
   res = []
   try:
      for i in x:
         res.append(w2vmodel[i])
   except Exception:
      return []
   return res

X1_train = []
X2_train = []
Y_train = []
allPhrases = set()

#random.shuffle(phrasePairs)
total = len(phrasePairs)
l = 0
fine = 0
err = 0
tot = 0
#f = open('./trainingset_siamese.txt', 'w')
print 'Loading training set'
for i in phrasePairs:
   l += 1
   i1 = transformData(i[0])
   i2 = transformData(i[1])
   sim = i[2]
   if(sim == True):
       sim = 1.0
   elif(sim == False):
       sim = 0.0
   if(i1 == [] or i2 == []):
      err += 1
   else:
      allPhrases.add(i[0])
      allPhrases.add(i[1])
      fine += 1
      ok = False
      X1_train.append(i1)
      X2_train.append(i2)
      Y_train.append(sim)
      #f.write("------------\n")
      #s1 = i[0]
      #s2 = i[1]
      #f.write('<'+s1[0]+'> <'+s1[1]+'> <'+s1[2]+'>\n')
      #f.write('<'+s2[0]+'> <'+s2[1]+'> <'+s2[2]+'>\n')
      #f.write(str(sim) + '\n')
      #f.write("------------\n")
   sys.stderr.write("   Processed %4d/%4d (%3.2f%%)\r" % (l, total, (float(l) / float(total)) * 100.00))
#f.close()
print
print 'Done loading training set'
print 'fine :', fine
print 'err  :', err
print 'total:', fine + err
print 'Size of training set:', len(Y_train)

#################################
print 'Freeing unused variables/memory'
#del Vocab
del phrasePairs
#################################

"""
#Some random sample(s) for checking
X1_train = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [2, 5, 6]]
X2_train = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15], [1, 3, 7]]
Y_train = [0.999, 1, 1.0, 1, 0.904, 0.02]
"""
X1_train = np.array(X1_train)
X2_train = np.array(X2_train)
Y_train = np.array(Y_train)

def cosine_distance(vects):
    x, y = vects
    x = K.l2_normalize(x, axis=-1)
    y = K.l2_normalize(y, axis=-1)
    return 1 - K.sum(x * y, axis=-1, keepdims=True)

def cos_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0],1)

def normalizedEmbeddings(x):
  return K.l2_normalize(x, axis = -1)

def normalizedEmbeddingsOutputShape(shapes):
  return (shapes[0], 1)

def euclidean_distance(vects):
  x, y = vects
  return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True))


def eucl_dist_output_shape(shapes):
  shape1, shape2 = shapes
  return (shape1[0], 1)

def contrastive_loss(y_true, y_pred):
  margin = 1
  return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))

ep = 0
class LossHistory(callbacks.Callback):
  def on_train_begin(self, logs={}):
    f = open('./log.out', 'w')
    f.close()
    self.losses = []
  def on_epoch_end(self, batch, logs={}):
    global ep
    p = random.randint(0, len(Y_train) - 10)
    q = random.randint(2, 5)
    f = open('./log.out','a+')
    f.write('========================================================================\n')
    f.write(str('Epoch:') + str(ep) + '\n')
    f.write('predicted similarity:' + str(list(self.model.predict([X1_train[p:p + q], X2_train[p:p + q]]))) + '\n')
    f.write('actual:' + str(Y_train[p:p + q]) + '\n')
    ep += 1

input_a = Input(shape = (3,100,))
input_b = Input(shape = (3,100,))

sharedNetwork = Sequential()
sharedNetwork.add(LSTM(128, return_sequences = True, input_shape=(3,100,)))
sharedNetwork.add(LSTM(128, return_sequences = True))
#sharedNetwork.add(LSTM(128, return_sequences = True))
sharedNetwork.add(LSTM(128))
sharedNetwork.add(Dense(100, init = 'glorot_normal', activation = 'tanh'))
#sharedNetwork.add(Lambda(normalizedEmbeddings, output_shape = normalizedEmbeddingsOutputShape))

processed_a = sharedNetwork(input_a)
#processed_a = Dense(128, activation = 'tanh', init = 'glorot_normal')(processed_a)
#processed_a = Dense(128, activation = 'tanh', init = 'glorot_normal')(processed_a)
#processed_a = LSTM(128, init = 'normal')(processed_a)

processed_b = sharedNetwork(input_b)
#processed_b = Dense(128, activation = 'tanh', init = 'glorot_normal')(processed_b)
#processed_b = Dense(128, activation = 'tanh', init = 'glorot_normal')(processed_b)
#processed_b = LSTM(128, init = 'normal')(processed_b)

#distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)([processed_a, processed_b])
distance = Lambda(cosine_distance, output_shape = cos_dist_output_shape)([processed_a, processed_b])

tb = TensorBoard(log_dir = './logs', histogram_freq = 0, write_graph = True)

model = Model(input = [input_a, input_b], output = [distance])
model1 = Model(input = [input_a], output = [processed_a])
model2 = Model(input = [input_b], output = [processed_b])

print 'Compiling model'
model.compile(loss = contrastive_loss, optimizer = RMSprop())
print 'Training'
history = LossHistory()
model.fit([X1_train, X2_train], [Y_train], batch_size = BATCH_SIZE, nb_epoch = NB_EPOCH, verbose = 1, callbacks = [history])

model.save('model_fr-en_siamese_' + lang + '.h5')
model1.save('model1_fr-en_siamese_' + lang + '.h5')
model2.save('model2_fr-en_siamese_' + lang + '.h5')

while True:
  try:
    ty = raw_input('type: ')
    if(ty == '1'):
      x = raw_input('Trigram: ')
      inp = x
      x = transformData(x.split(' '))
      X1 = np.array([x])
      X2 = []
      tmp = []
      print model1.predict([X1])
      for i in allPhrases:
        X2 = [transformData(i)]
        X2 = np.array(X2)
        sim = model.predict([X1, X2])
        tmp.append((sim[0], i))
      tmp.sort()
      inp = inp.split(' ')
      print 'given trigram:', inp[0], inp[1], inp[2]
      for i in xrange(6):
        print tmp[i][1], tmp[i][0]
    else:
      x = raw_input('Trigram1: ')
      y = raw_input('Trigram2: ')
      x = transformData(x.split(' '))
      y = transformData(y.split(' '))
      X1 = np.array([x])
      X2 = np.array([y])
      sim = model.predict([X1, X2])
      print 'similarity:', sim[0]
  except Exception as e:
    print e
"""
print model1.predict(np.array([[1, 2, 3]]))
print model2.predict(np.array([[1, 2, 3]]))

print model1.predict(np.array([[4, 5, 6]]))
print model2.predict(np.array([[4, 5, 6]]))

print model1.predict(np.array([[7, 8, 9]]))
print model2.predict(np.array([[7, 8, 9]]))

print model.predict([X1_train, X2_train])

exit(0)
"""
