#!/usr/bin/python

import os
import sys
import random
from math import log
from math import sqrt
import pickle
import nltk
from nltk.corpus import stopwords
########################
# Check the number of phrases.
########################

minimumTrigramsCount = None
ignore = ['', '.', ',', '&', '"', "'", '(', ')', '-', '=', '+', '-lrb-', '-rrb-', '`', '/', ':', ';', '&apos;']
data = None
unigramsCount = {}
allJointCount = 0
allTrigramsCount = 0
allFeaturesCount = 0
allJointCount = 0
allFiveGrams = set()
trigramsCount = {}
featuresCount = {}
jointCount = {}
allTriGrams = set()
allFeatures = {}
PMI = {}
similarity = {}
allowedNeighbours = 20

try:
   fname = sys.argv[1]
   minimumTrigramsCount = int(sys.argv[2])
   maximumUnigramsCount = int(sys.argv[3])
   allowedNeighbours = int(sys.argv[4])
   outputFile = sys.argv[5]
except Exception:
   print 'Usage: python <makeGraph.py> <data file> <minimum trigrams count> <maximum unigram count> <allowed neighbours> <output file>'
   exit(0)
print 'Data file:', fname
print 'Considering trigrams of count >=', minimumTrigramsCount
print 'Maximum allowed unigram count', maximumUnigramsCount
print 'print allowed neighbours:', allowedNeighbours
print 'output file:', outputFile

def resetFile(fname):
   print 'Clearning previous saved data'
   f = open(fname, 'w')
   f.close()

def readData(fname = None):
   if(fname == None):
      print 'File name missing'
      exit(0)
   print 'Reading data from', fname
   f = open(fname, 'r')
   x = f.read()
   f.close()
   return x.lower()

def getFeatures(x):
   assert(len(x) == 5)
   res = []
   res.append(x)
   res.append((x[1], x[2], x[3]))
   res.append((x[0], x[1]))
   res.append((x[3], x[4]))
   res.append((x[2]))
   res.append((x[1], x[3]))
   res.append((x[1], x[3], x[4]))
   res.append((x[0], x[1], x[3]))
   return res

def checkNGram(x):
   for i in ignore:
      if(i in x):
         return False
   for i in x:
      if(unigramsCount[i] >= maximumUnigramsCount):
         return False
   return True

def checkNumber(x, y, epsilon):
  if -epsilon <= x - y <= epsilon:
    return True

  if -epsilon <= x <= epsilon or -epsilon <= y <= epsilon:
    return False

  return (-epsilon <= (x - y) / x <= epsilon
       or -epsilon <= (x - y) / y <= epsilon)

allCount = 0

def calculatePMI(tg, f):
   global allCount
   p1 = jointCount[(tg, f)] * allCount
   p2 = trigramsCount[tg] * featuresCount[f]
   return log(p1, 2) - log(p2, 2)
   #p1 = jointCount[(tg, f)] / float(allJointCount)
   #p1 = jointCount[(tg, f)] / float(trigramsCount[tg])
   #p2 = trigramsCount[tg] / float(allTrigramsCount)
   #p3 = featuresCount[f] / float(allFeaturesCount)
   #return log(p1, 2) - log(p2, 2) - log(p3, 2)
   #return log(p1, 2) - log(p3, 2)


data = readData(fname)
data = data.split('\n')

total = len(data)
print 'Extracting features'

for i in xrange(total):
   x = data[i].split(' ')
   for j in x:
      try:
         unigramsCount[j] += 1
      except Exception:
         unigramsCount[j] = 1

for i in xrange(total):
   x = data[i].split(' ')
   L = len(x)
   for j in xrange(L):
      leftContext = ''
      if(j - 2 >= 0):
         leftContext = x[j - 2]
      left = ''
      if(j - 1 >= 0):
         left = x[j - 1]
      mid = x[j]
      right = ''
      if(j + 1 < L):
         right = x[j + 1]
      rightContext = ''
      if(j + 2 < L):
         rightContext = x[j + 2]
      fiveGram = (leftContext, left, mid, right, rightContext)
      if(not checkNGram(fiveGram)):
         #print fiveGram
         continue
      triGram = (left, mid, right)
      try:
         _ = trigramsCount[triGram]
         trigramsCount[triGram] += 1
      except KeyError:
         trigramsCount[triGram] = 1
      allCount += 1

for i in xrange(total):
   x = data[i].split(' ')
   L = len(x)
   for j in xrange(L):
      leftContext = ''
      if(j - 2 >= 0):
         leftContext = x[j - 2]
      left = ''
      if(j - 1 >= 0):
         left = x[j - 1]
      mid = x[j]
      right = ''
      if(j + 1 < L):
         right = x[j + 1]
      rightContext = ''
      if(j + 2 < L):
         rightContext = x[j + 2]
      fiveGram = (leftContext, left, mid, right, rightContext)
      if(not checkNGram(fiveGram)):
         continue 
      triGram = (left, mid, right)
      if(trigramsCount[triGram] < minimumTrigramsCount):
         continue
      allTriGrams.add(triGram)
      allTrigramsCount += 1
      allFiveGrams.add(fiveGram)
      features = getFeatures(fiveGram)
      try:
         _ = allFeatures[triGram]
      except KeyError:
         allFeatures[triGram] = set()
      for k in features:
         allFeatures[triGram].add(k)
         allFeaturesCount += 1
         try:
            _ = featuresCount[k]
            featuresCount[k] += 1
         except KeyError:
            featuresCount[k] = 1
         try:
            _ = jointCount[(triGram, k)]
            jointCount[(triGram, k)] += 1
         except KeyError:
            jointCount[(triGram, k)] = 1
         allJointCount += 1
         allCount += 2
   sys.stderr.write('  Processed %05d/%05d (%.2f%%)     \r' % (i + 1, total, (float(i + 1) / float(total)) * 100.0))
   sys.stderr.flush()

allCount = allTrigramsCount + allJointCount + allFeaturesCount
print
print 'Calculating PMI for all trigrams'
total = len(allTriGrams)
l = 0
for i in allTriGrams:
   PMI[i] = {}
   l += 1
   for j in allFeatures[i]:
      pmi = calculatePMI(i, j)
      PMI[i][j] = pmi
   sys.stderr.write('  Processed %05d/%05d (%.2f%%)     \r' % (l, total, (float(l) / float(total)) * 100.0))
   sys.stderr.flush()
print
print 'Calculating similarity between all trigrams'
total = len(allTriGrams)
l = 0
count = 0
for i in allTriGrams:
   if(trigramsCount[i] >= minimumTrigramsCount):
      count += 1

phrasePairs = {}
for i in allTriGrams:
   if(trigramsCount[i] < minimumTrigramsCount):
      assert(False)
   f1 = allFeatures[i]
   cur = []
   l += 1
   phrasePairs[i] = []
   for j in allTriGrams:
      if(trigramsCount[j] < minimumTrigramsCount):
         assert(False)
      sim = 0
      a = 0
      b = 0
      c = 0
      for k in f1:
         try:
            p = PMI[i][k]
            q = PMI[j][k]
            sim += (p * q)
            a += (p * p)
            b += (q * q)
            c += 1
         except KeyError:
            pass
      if(c <= 1 or a == 0 or b == 0):
         sim = 0
      else:
         a = sqrt(a)
         b = sqrt(b)
         sim = (float(sim) / float(a * b))
      dist = 1 - sim
      out = -1
      if(dist >= 0.2):
         out = 0
      else:
         out = 1
      cur.append((out, j))
   cur.sort(reverse = True)
   L = len(cur)
   assert(L == count)
   for j in xrange(L):
      phrasePairs[i].append((cur[j][1], cur[j][0]))
   sys.stderr.write('  Processed %05d/%05d (%.2f%%)     \r' % (l, total, (float(l) / float(total)) * 100.0))
   sys.stderr.flush()

fn = open(outputFile, 'wb')
pickle.dump(phrasePairs, fn)
fn.close()
print 'DONE'
