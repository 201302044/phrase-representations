#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os.path
import sys
import multiprocessing
from gensim.models import Word2Vec
 
if __name__ == '__main__':
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)
 
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
    logging.root.setLevel(level=logging.INFO)
    logger.info("running %s" % ' '.join(sys.argv))
 
    # check and process input arguments
 
    if len(sys.argv) < 3:
        print globals()['__doc__'] % locals()
        sys.exit(1)
    inp, outp = sys.argv[1:3]
    sent = []
    f = open(inp)
    sent = f.read()
    f.close()
    sent = sent.split('\n')
    L = len(sent)
    for i in xrange(L):
        sent[i] = sent[i].lower()
        sent[i] = sent[i].split(' ')
    print sent[0]
    print sent[-1]
    #print sent[1]
    #exit(0)
    #print sent
    #exit(0)
    model = Word2Vec(sent, size=100, window=5, min_count=5, workers=multiprocessing.cpu_count())
 
    # trim unneeded model memory = use (much) less RAM
    model.init_sims(replace=True)
 
    model.save(outp)
